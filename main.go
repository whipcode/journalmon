package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
)

func main() {
	// Retrieve the parameters provided on the command-line
	var (
		host      string
		port      int
		statsfile string
		rulefile  string
		logfile   string
	)
	flag.StringVar(&host, "host", "localhost", "systemd-journal-gatewayd host")
	flag.IntVar(&port, "port", 19531, "systemd-journal-gatewayd port")
	flag.StringVar(&statsfile, "statsfile", "stats.json", "path to stats data file")
	flag.StringVar(&rulefile, "rulefile", "rules.json", "path to the rules file")
	flag.StringVar(&logfile, "logfile", "/var/log/filtered.log", "path to the filtered log")
	flag.Parse()

	// Create a channel for unfiltered/filtered entries
	unf := make(chan *Entry, 10)
	fil := make(chan *Entry, 10)

	// Create the filter, attempting to load the rules from disk
	f, err := NewFilter(rulefile, unf, fil)
	if err != nil {
		log.Fatal(err)
	}

	// Create a Stats for keeping track of the cursor,
	// a Reader for reading from the journal, and a
	// Writer for writing the filtered entries to disk
	stats := NewStats(statsfile)
	r := NewReader(host, port, stats, unf)
	w, err := NewWriter(logfile, stats, fil)
	if err != nil {
		log.Fatal(err)
	}
	defer w.Close()

	// Spawn a goroutine for stat sync'ing and three for the "chain"
	go stats.PeriodicallySync()
	go r.ReadEntries()
	go f.FilterEntries()
	go w.WriteEntries()

	// Wait for SIGINT (typically Ctrl+C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	// Sync the stats one last time
	if err := stats.Sync(); err != nil {
		log.Fatal(err)
	}
}
