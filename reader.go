package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

// Storage for internal state variables
type Reader struct {
	client *http.Client
	url    string
	stats  *Stats
	unf    chan<- *Entry
}

// Create a new Reader
func NewReader(host string, port int, stats *Stats, unf chan<- *Entry) *Reader {
	u := url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("%s:%d", host, port),
		Path:   "/entries",
	}
	return &Reader{
		client: &http.Client{},
		url:    u.String(),
		stats:  stats,
		unf:    unf,
	}
}

// Retrieve raw list of entries from the API
func (r Reader) pollAPI() ([]byte, error) {
	req, err := http.NewRequest("GET", r.url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Range", "entries="+r.stats.Cursor+":1000")
	resp, err := r.client.Do(req)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(resp.Body)
}

// Retrieve the next entries from the journal
func (r *Reader) readEntries() error {
	b, err := r.pollAPI()
	if err != nil {
		return err
	}
	for _, l := range bytes.Split(bytes.TrimSpace(b), []byte{'\n'}) {
		e := &Entry{}
		if err := json.Unmarshal(l, e); err != nil {
			return err
		}
		r.unf <- e
		r.stats.Cursor = e.Cursor
		r.stats.NumRead++
	}
	return nil
}

// Read entries from the journal, pausing for two seconds each time
func (r *Reader) ReadEntries() {
	for {
		err := r.readEntries()
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(2 * time.Second)
	}
}
