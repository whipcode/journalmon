package main

import (
	"encoding/json"
	"io/ioutil"
)

// Storage for the rules
type Filter struct {
	rules []Rule
	unf   <-chan *Entry
	fil   chan<- *Entry
}

// Create a filter from a rulefile
func NewFilter(rulefile string, unf <-chan *Entry, fil chan<- *Entry) (*Filter, error) {
	b, err := ioutil.ReadFile(rulefile)
	if err != nil {
		return nil, err
	}
	f := &Filter{
		unf: unf,
		fil: fil,
	}
	if err := json.Unmarshal(b, &f.rules); err != nil {
		return nil, err
	}
	for i, _ := range f.rules {
		if err := f.rules[i].Initialize(); err != nil {
			return nil, err
		}
	}
	return f, nil
}

// Filter entries as they are received
func (f Filter) FilterEntries() {
Loop:
	for e := range f.unf {
		for _, r := range f.rules {
			if r.Matches(e) {
				continue Loop
			}
		}
		f.fil <- e
	}
}
