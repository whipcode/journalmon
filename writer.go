package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

type Writer struct {
	file  *os.File
	stats *Stats
	fil   <-chan *Entry
}

// Log levels used by journald
var LogLevels = []string{
	"EMERG",
	"ALERT",
	"CRIT",
	"ERR",
	"WARNING",
	"NOTICE",
	"INFO",
	"DEBUG",
}

// Open the logfile for writing
func NewWriter(logfile string, stats *Stats, fil <-chan *Entry) (*Writer, error) {
	f, err := os.OpenFile(logfile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}
	w := &Writer{
		file:  f,
		stats: stats,
		fil:   fil,
	}
	return w, nil
}

// Write entries to the logfile as they come in
func (w *Writer) WriteEntries() {
	for e := range w.fil {
		t := time.Unix(0, e.Timestamp*1000)
		_, err := fmt.Fprintf(
			w.file,
			"%s [%s] %s\n",
			t.Format("Jan 2, 2006 15:04:05"),
			LogLevels[e.Priority],
			e.Message,
		)
		if err != nil {
			log.Fatal(err)
		}
		w.stats.NumWritten++
	}
}

// Close the log
func (w *Writer) Close() {
	w.file.Close()
}
