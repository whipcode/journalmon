## Journal Monitor

A tool for filtering journald entries and writing the ones that do not match a predefined list of filters to a logfile.

### Usage

Journal Monitor is invoked with any of the following parameters:

    journalmon \
        -host localhost \
        -port 19531 \
        -statsfile stats.json \
        -rulefile rules.json \
        -logfile /var/log/filtered

* `host` & `port` - designate the hostname/IP address and port of the [systemd journal gateway](http://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html)
* `statsfile` - path to a JSON file used for storing the journal cursor and basic statistics about filtering (must be writable)
* `rulefile` - path to a JSON file containing a list of rules to apply (see below)
* `logfile` - path to a file on disk that the filtered log entries will be appended to (must also be writable)

All of these options have sensible defaults.

### Rules

The JSON file containing the rules should be in the following format:

    [
      {
        "message":      "^Reloading",
        "identifier":   "nginx",
        "unit":         "nginx.service",
        "min_priority": "EMERG",
        "max_priority": "WARNING"
      },
      {
        ...
      }
    ]

The file consists of a list with zero or more rule objects. Each rule consists of three regular expression strings (`message`, `identifier`, and `unit`) and two strings representing the minimum and maximum syslog levels (`EMERG`, `ALERT`, `CRIT`, `ERR`, `WARNING`, `NOTICE`, `INFO`, or `DEBUG`).

Any entry that matches **all** of the items in each rule will be discarded by the filter. Everything else will continue to be written to the log.
