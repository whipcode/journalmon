package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"time"
)

// Statistics gathered during operation
type Stats struct {
	Cursor     string `json:"cursor"`
	NumRead    int64  `json:"num_read"`
	NumWritten int64  `json:"num_written"`
	filename   string
}

// Load stats from disk if possible or create new stats
func NewStats(statsfile string) *Stats {
	var stats = &Stats{filename: statsfile}
	if b, err := ioutil.ReadFile(statsfile); err == nil {
		json.Unmarshal(b, stats)
	}
	return stats
}

// Write the stats to disk
func (s Stats) Sync() error {
	b, err := json.Marshal(s)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(s.filename, b, 0644)
}

// Write the stats to disk every so often (once a minute)
func (s Stats) PeriodicallySync() {
	c := time.Tick(time.Minute)
	for _ = range c {
		if err := s.Sync(); err != nil {
			log.Fatal(err)
		}
	}
}
