package main

import (
	"errors"
	"regexp"
)

// Description of patterns and parameters of messages to reject
type Rule struct {
	Message           string `json:"message"`
	Identifier        string `json:"identifier"`
	Unit              string `json:"unit"`
	MinPriority       string `json:"min_priority"`
	MaxPriority       string `json:"max_priority"`
	messagePattern    *regexp.Regexp
	identifierPattern *regexp.Regexp
	unitPattern       *regexp.Regexp
	minPriority       int
	maxPriority       int
}

// Priority contants
var priorities = map[string]int{
	"EMERG":   0,
	"ALERT":   1,
	"CRIT":    2,
	"ERR":     3,
	"WARNING": 4,
	"NOTICE":  5,
	"INFO":    6,
	"DEBUG":   7,
}

// Compile the patterns in the rule and convert the integer values
func (r *Rule) Initialize() (err error) {
	if r.Message != "" {
		if r.messagePattern, err = regexp.Compile(r.Message); err != nil {
			return
		}
	}
	if r.Identifier != "" {
		if r.identifierPattern, err = regexp.Compile(r.Identifier); err != nil {
			return
		}
	}
	if r.Unit != "" {
		if r.unitPattern, err = regexp.Compile(r.Unit); err != nil {
			return
		}
	}
	if r.MinPriority != "" {
		min, ok := priorities[r.MinPriority]
		if !ok {
			return errors.New("value for MinPriority is invalid")
		}
		r.minPriority = min
	}
	if r.MaxPriority != "" {
		max, ok := priorities[r.MaxPriority]
		if !ok {
			return errors.New("value for MaxPriority is invalid")
		}
		r.maxPriority = max
	} else {
		r.maxPriority = len(priorities) - 1
	}
	return
}

// Determine if the specified entry matches the rule
func (r *Rule) Matches(e *Entry) bool {
	if r.messagePattern != nil && !r.messagePattern.Match(e.Message) {
		return false
	}
	if r.identifierPattern != nil && !r.identifierPattern.Match(e.Identifier) {
		return false
	}
	if r.unitPattern != nil && !r.unitPattern.Match(e.Unit) {
		return false
	}
	if e.Priority < r.minPriority || e.Priority > r.maxPriority {
		return false
	}
	return true
}
