package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
)

// Journal entry received from the API
type Entry struct {
	Cursor     string
	Message    []byte
	Identifier []byte
	Unit       []byte
	Timestamp  int64
	Priority   int
}

/*
 * This is a bit more complicated than it needs to be because some entries are
 * provided as a string and other times provided as a []byte. The integer values
 * are converted to different types, but must be converted to a string first.
 * The error checking is very comprehensive and verbose.
 */

// Load the cursor from the entry
func (e *Entry) setCursor(values map[string]interface{}) error {
	v, ok := values["__CURSOR"]
	if !ok {
		return errors.New("__CURSOR missing from entry")
	}
	c, ok := v.(string)
	if !ok {
		return errors.New("__CURSOR is not a string")
	}
	e.Cursor = c
	return nil
}

// Load the message from the entry
func (e *Entry) setMessage(values map[string]interface{}) error {
	m, ok := values["MESSAGE"]
	if !ok {
		return errors.New("MESSAGE missing from entry")
	}
	switch m.(type) {
	case []interface{}:
		s := m.([]interface{})
		e.Message = make([]byte, len(s))
		for i, v := range s {
			n, ok := v.(json.Number)
			if !ok {
				return errors.New(fmt.Sprintf("MESSAGE slice type is %T", v))
			}
			c, err := n.Int64()
			if err != nil {
				return err
			}
			e.Message[i] = uint8(c)
		}
	case string:
		e.Message = []byte(m.(string))
	default:
		return errors.New(fmt.Sprintf("MESSAGE type is %T", m))
	}
	return nil
}

// Load the identifier if present
func (e *Entry) setIdentifier(values map[string]interface{}) error {
	if v, ok := values["SYSLOG_IDENTIFIER"]; ok {
		c, ok := v.(string)
		if !ok {
			return errors.New("SYSLOG_IDENTIFIER is not a string")
		}
		e.Identifier = []byte(c)
	}
	return nil
}

// Load the unit if present
func (e *Entry) setUnit(values map[string]interface{}) error {
	if v, ok := values["_SYSTEMD_UNIT"]; ok {
		c, ok := v.(string)
		if !ok {
			return errors.New("_SYSTEMD_UNIT is not a string")
		}
		e.Unit = []byte(c)
	}
	return nil
}

// Load the timestamp from the entry
func (e *Entry) setTimestamp(values map[string]interface{}) error {
	v, ok := values["__REALTIME_TIMESTAMP"]
	if !ok {
		return errors.New("__REALTIME_TIMESTAMP missing from entry")
	}
	c, ok := v.(string)
	if !ok {
		return errors.New("__REALTIME_TIMESTAMP is not a string")
	}
	i, err := strconv.ParseInt(c, 10, 64)
	if err != nil {
		return err
	}
	e.Timestamp = i
	return nil
}

// Load the priority from the entry
func (e *Entry) setPriority(values map[string]interface{}) error {
	v, ok := values["PRIORITY"]
	if !ok {
		return errors.New("PRIORITY missing from entry")
	}
	c, ok := v.(string)
	if !ok {
		return errors.New("PRIORITY is not a string")
	}
	i, err := strconv.ParseInt(c, 10, 8)
	if err != nil {
		return err
	}
	e.Priority = int(i)
	return nil
}

// Parse JSON data into entry
func (e *Entry) UnmarshalJSON(b []byte) error {
	d := json.NewDecoder(bytes.NewBuffer(b))
	d.UseNumber()
	var v map[string]interface{}
	if err := d.Decode(&v); err != nil {
		return err
	}
	fns := []func(map[string]interface{}) error{
		e.setCursor, e.setMessage, e.setIdentifier, e.setUnit, e.setTimestamp, e.setPriority,
	}
	for _, fn := range fns {
		if err := fn(v); err != nil {
			return err
		}
	}
	return nil
}
