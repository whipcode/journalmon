package main

import (
	"regexp/syntax"
	"testing"
)

func TestCompilePatterns(t *testing.T) {
	r := &Rule{
		Message:    "abc",
		Identifier: "",
		Unit:       "+",
	}
	err := r.Initialize()

	// The first regexp should not be nil
	if r.messagePattern == nil {
		t.Error("r.messagePattern should not be nil")
	}

	// The socond regexp should be nil since it is blank
	if r.identifierPattern != nil {
		t.Error("r.identifierPattern should be nil")
	}

	// The final regexp should have triggered an error
	if _, ok := err.(*syntax.Error); !ok {
		t.Error("r.Unit should have generated a syntax error")
	}
}

func TestMatchMessage(t *testing.T) {
	r := &Rule{Message: "b"}
	if err := r.Initialize(); err != nil {
		t.Error(err)
	}
	e := &Entry{Message: []byte{'1', '2', '3', '4'}}
	if r.Matches(e) {
		t.Error("e unexpectedly matched rule")
	}
	e.Message = []byte{'a', 'b', 'c', 'd'}
	if !r.Matches(e) {
		t.Error("e did not match rule")
	}
}

func TestMatchMultiple(t *testing.T) {
	r := &Rule{
		Message:    "^abc",
		Identifier: "^abc",
	}
	if err := r.Initialize(); err != nil {
		t.Error(err)
	}
	e := &Entry{
		Message:    []byte{'a', 'b', 'c'},
		Identifier: []byte{'1', '2', '3'},
	}
	if r.Matches(e) {
		t.Error("e unexpectedly matched rule")
	}
	e.Identifier = []byte{'a', 'b', 'c'}
	if !r.Matches(e) {
		t.Error("e did not match rule")
	}
}

func TestMatchPriority(t *testing.T) {
	r := &Rule{
		MinPriority: "WARNING",
		MaxPriority: "INFO",
	}
	if err := r.Initialize(); err != nil {
		t.Error(err)
	}
	e := &Entry{Priority: 7}
	if r.Matches(e) {
		t.Error("e unexpectedly matched rule")
	}
	e.Priority = 5
	if !r.Matches(e) {
		t.Error("e did not match rule")
	}
}
